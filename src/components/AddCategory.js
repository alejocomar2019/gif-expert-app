import React, { useState } from 'react'
import PropTypes from 'prop-types'

const AddCategory = ({ categories, setCategories }) => {

  // State
  const [inputValue, setInputValue] = useState('')

  // Functions

  const handleInputChange = (e) => {
    const { target: { value } } = e
    setInputValue(value)
  }

  const handleSubmit = (e) => {

    e.preventDefault()

    if (!validateForm()) return

    // Emit set categories event to parent component
    setCategories((cats) => [inputValue, ...cats])
    // Clearing inputValue
    setInputValue('')

  }

  const validateForm = () => {
    let isValid = true
    if (!inputValue || !inputValue.trim()) {
      isValid = false
    } else if (categories.find(cat => cat.toLowerCase() === inputValue.toLowerCase())) {
      console.log('Value already exists in categories', inputValue)
      // alert('This category already exists')
      isValid = false
    }
    return isValid
  }

  return (
    <form onSubmit={ handleSubmit }>
      <input
        type="text"
        value={ inputValue }
        placeholder="Write a category"
        onChange={ handleInputChange }
      />
    </form>
  )
}

export default AddCategory

AddCategory.defaultProps = {
  categories: []
}

AddCategory.propTypes = {
  categories: PropTypes.array,
  setCategories: PropTypes.func.isRequired
}


