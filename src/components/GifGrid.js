import React from 'react'
import { useFetchGifs } from '../hooks/useFetchGifs'
import PropTypes from 'prop-types'
import GifGridItem from './GifGridItem'

const GifGrid = ({ category }) => {

  const { data: gifs, loading } = useFetchGifs(category)

  return (
    <>
      <h3 className="animate__animated animate__fadeIn animate__fadeIn">
        { category }
      </h3>

      { loading && <p>Cargando...</p> }

      <div className="card-grid">
        {
          gifs.map((gif, index) =>
            <GifGridItem
              key={`${gif.title}-${index}`}
              title={gif.title}
              url={gif.url}
            />
          )
        }
      </div>
    </>
  )
}

GifGrid.propTypes = {
  category: PropTypes.string.isRequired
}

export default GifGrid
