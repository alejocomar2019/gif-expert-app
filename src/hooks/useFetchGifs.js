import { useState, useEffect } from 'react'
import { getGifs } from '../services/gifs'

export const useFetchGifs = (category) => {
  const [state, setState] = useState({
    data: [],
    loading: true
  })

  useEffect(() => {
    getGifs(category)
      .then(response => {
        const data = response.map(gif => {
          const { id, title, images } = gif
          return { id, title, url: images?.downsized_medium.url }
        })
        setState({data, loading: false})
      })
      .catch(e => {
        setState({data: [], loading: false})
      })
  })

  return state

}
