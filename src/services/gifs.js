import axios from 'axios'
import { giphyUrlBase, giphyApiKey } from './config'

export const getGifs = async (category) => {
  try {
    const request = {
      method: 'GET',
      params: {
        api_key: giphyApiKey,
        q: encodeURI(category),
        limit: 10
      },
      url: giphyUrlBase
    }
    const { data: { data } } = await axios(request)
    return data
  } catch (e) {
    console.error(e)
    throw new Error(e.message)
  }

}
