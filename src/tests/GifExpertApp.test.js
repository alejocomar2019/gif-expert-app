import React from 'react'
import { shallow } from 'enzyme'
import GifExpertApp from '../GifExpertApp'

describe('Tests of src/GifExpertApp.js', () => {

  test('Component must render correctly', () => {

    const wrapper = shallow( <GifExpertApp/> )

    expect(wrapper).toMatchSnapshot()

  })

  test('Must render categories items', () => {

    const categories = ['One Punch', 'Samurai X', 'Dragon Ball']

    const wrapper = shallow( <GifExpertApp defaultCategories={ categories } /> )

    expect(wrapper).toMatchSnapshot()
    expect( wrapper.find('GifGrid').length ).toBe(categories.length)

  })

})
