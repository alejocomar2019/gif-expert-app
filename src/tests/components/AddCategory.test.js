import React from 'react'
import { shallow } from 'enzyme'
import AddCategory from '../../components/AddCategory'

describe('Tests in src/components/AddCategory.js', () => {

  const categories = ['One Punch', 'Samurai X', 'Dragon Ball']

  const setCategories = jest.fn()

  let wrapper

  beforeEach(() => {
    jest.clearAllMocks()
    // Clearing component beforeEach test
    wrapper = shallow(
      <AddCategory
        categories={categories}
        setCategories={setCategories}
      />
    )

  })

  test('Component must render successfully', () => {

    expect(wrapper).toMatchSnapshot()

  })

  test('Input value must change', () => {

    const value = 'Hello World!'

    // Simulating change event and passing the change event
    wrapper.find('input').simulate('change', { target: { value } })

    const inputValue = wrapper.find('input').prop('value')

    expect( inputValue ).toBe( value )

  })

  test('Form must not be submitted if inputValue is falsy', () => {

    wrapper.find('form').simulate('submit', { preventDefault(){} })

    expect( setCategories ).not.toHaveBeenCalled()

  })

  test('Form must not be submitted if inputValue currently exists in categories', () => {

    const value = categories[0]

    // Simulating change event and passing the change event
    wrapper.find('input').simulate('change', { target: { value } })

    wrapper.find('form').simulate('submit', { preventDefault(){} })

    expect( setCategories ).not.toHaveBeenCalled()

  })

  test('Must call setCategories function if a correct value was submitted and clear the input value', () => {

    const value = 'Rick And Morty'

    wrapper.find('input').simulate('change', { target: { value } })

    wrapper.find('form').simulate('submit', { preventDefault(){} })

    expect( setCategories ).toHaveBeenCalled()

    expect( wrapper.find('input').prop('value') ).toBe('')

  })

})
