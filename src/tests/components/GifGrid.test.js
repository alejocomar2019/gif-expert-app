import React from 'react'
import { shallow } from 'enzyme'
import GifGrid from '../../components/GifGrid'
import { useFetchGifs } from '../../hooks/useFetchGifs'
jest.mock('../../hooks/useFetchGifs')

describe('Tests of /src/components/GifGrid.js', () => {

  const category = 'Rick and Morty'

  test('Component must render successfully', () => {

    useFetchGifs.mockReturnValue({
      data: [],
      loading: true
    })

    const wrapper = shallow( <GifGrid category={ category }  /> )

    expect( wrapper.find('p').exists() ).toBe(true)

    expect(wrapper).toMatchSnapshot()

  })

  test('Must show items when useFetch returns the information', () => {

    const gifs = [
      { id: 'ABC', url: 'https://asdojosad.com', title: 'Cualquier cosa' }
    ]

    useFetchGifs.mockReturnValue({
      data: gifs,
      loading: false
    })

    const wrapper = shallow( <GifGrid category={ category }  /> )

    expect(wrapper).toMatchSnapshot()
    expect( wrapper.find('p').exists() ).toBe(false)
    expect( wrapper.find('GifGridItem').length ).toBe( gifs.length )

  })

})
