import React from 'react'
import GifGridItem from '../../components/GifGridItem'
import { shallow } from 'enzyme'

describe('Testing GifGridItem component', () => {

  const gif = { title:'Title', url: 'someurl' }
  const wrapper = shallow(
    <GifGridItem
      title={gif.title}
      url={gif.url}
    />
  )

  test('Must show the component correctly', () => {

    expect(wrapper).toMatchSnapshot()

  })

  test('The paragraph test must be equal to the title', () => {

    const paragraphText = wrapper.find('p').text().trim()
    expect(paragraphText).toBe(gif.title)

  })

  test('The image src and alt attributes must be equal to the url and title props respectively', () => {

    const img = wrapper.find('img')
    const imageProps = img.props()
    // img.prop('src') Returns the src of the image
    const { src, alt } = imageProps

    expect(alt).toBe(gif.title)
    expect(src).toBe(gif.url)

  })

  test('The container div must have the classes card animate__animated animate__fadeIn', () => {

    const containerDiv = wrapper.find('.animate__animated.animate__fadeIn')
    expect(containerDiv.exists()).toBe(true)

  })

})
