import { useFetchGifs } from '../../hooks/useFetchGifs'
import { renderHook } from '@testing-library/react-hooks'

describe('Tests in useFetchGifs', () => {

  test('Must return the initial state', async () => {

    const category = 'One Punch'

    const { result, waitForNextUpdate } = renderHook(() => useFetchGifs(category))

    const { data, loading } = result.current

    await waitForNextUpdate()

    expect(data).toEqual([])
    expect( loading ).toBe( true )

  })

  test('Must return an images array and loading in false', async () => {

    const category = 'One Punch'

    const { result, waitForNextUpdate } = renderHook(() => useFetchGifs(category))

    await waitForNextUpdate()

    const { data, loading } = result.current

    expect(data.length).toBe(10)
    expect( loading ).toBe( false )

  })

})
