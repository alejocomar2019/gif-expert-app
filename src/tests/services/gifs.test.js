import { getGifs } from '../../services/gifs'

describe('Testing gifs services', () => {

  test('getGifs service must return an array with 10 elements', async () => {

    const category = 'One Punch'
    const gifs = await getGifs(category)

    expect(gifs).toBeDefined()
    expect(Array.isArray(gifs)).toBe(true)
    expect(gifs.length).toBe(10)

  })

  test('getGifs must return an empty array if no category was passed', async () => {

    const gifs = await getGifs('')

    expect(gifs).toBeDefined()
    expect(Array.isArray(gifs)).toBe(true)
    expect(gifs.length).toBe(0)
    
  })

})
